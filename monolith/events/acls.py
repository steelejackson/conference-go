import requests
from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY


def get_picture_url(query):
    '''
    get the url of a picture from the pexels API
    '''
    url = f'https://api.pexels.com/v1/search?query={query}'

    headers = {
        'Authorization': PEXELS_API_KEY
    }
    
    response = requests.get(url, headers=headers)

    api_dict = response.json()
    
    return api_dict['photos'][0]['src']['original']

def get_lat_and_lon(city, state):
    url = f'http://api.openweathermap.org/geo/1.0/direct?q={city},{state},{"ISO 3166-2:US"}&appid={OPEN_WEATHER_API_KEY}'
    
    response = requests.get(url)

    lat_lon_dict = response.json()

    #print(f'{city}, {state}')

    return(lat_lon_dict[0]['lat'], lat_lon_dict[0]['lon'])

def kelvin_to_f(k):
    return (k-273.15) * 9/5 + 32


def weather(city, state):
    lat_lon_url = f'http://api.openweathermap.org/geo/1.0/direct?q={city},{state},{"ISO 3166-2:US"}&appid={OPEN_WEATHER_API_KEY}'

    #set latitude/longitute url
    
    #convert response to json
    response = requests.get(lat_lon_url)
    lat_lon_dict = response.json()

    #set variables representing latitude and longitude

    latitude = lat_lon_dict[0]['lat']
    longitude = lat_lon_dict[0]['lon']

    #set url for weather API

    weather_url = f'https://api.openweathermap.org/data/2.5/weather?lat={latitude}&lon={longitude}&appid={OPEN_WEATHER_API_KEY}'

    #get response
    weather_response = requests.get(weather_url)
    #convert response to json
    weather_dict = weather_response.json()
    #create dictionary with the current weather conditions
    weather = {
        'conditions': f'{weather_dict["weather"][0]["main"]}',
        'temperature': f'{kelvin_to_f(weather_dict["main"]["temp"]).__round__()} degrees'
    }

    return weather





    


from django.http import JsonResponse

from .models import Conference, Location

from common.json import ModelEncoder

from django.views.decorators.http import require_http_methods

from events.models import State

import json

from .acls import get_picture_url, weather




class ConferenceListEncoder(ModelEncoder):
    model = Conference
    properties = [
        'name'
    ]


@require_http_methods(['GET', 'POST'])
def api_list_conferences(request):
    """
    Lists the conference names and the link to the conference.

    Returns a dictionary with a single key "conferences" which
    is a list of conference names and URLS. Each entry in the list
    is a dictionary that contains the name of the conference and
    the link to the conference's information.

    {
        "conferences": [
            {
                "name": conference's name,
                "href": URL to the conference,
            },
            ...
        ]
    }
    """
    if request.method == 'GET':
        conferences = Conference.objects.all()
        return JsonResponse(
            {"conferences": conferences},
            encoder = ConferenceV0DetailEncoder)
    else:
        content = json.loads(request.body)

        #get the location and put it in the content dict

        try:
            location = Location.objects.get(id=content['location'])
            content['location'] = location
        except Location.DoesNotExist:
            return JsonResponse(
                {'message': 'Invalid location id'},
                status=400,
            )
        #create the new conference with the implemented location foreign key
        conference = Conference.objects.create(**content)
        return JsonResponse(
            conference,
            encoder=ConferenceDetailEncoder,
            safe=False,
        )





class LocationListEncoder(ModelEncoder):
    model = Location
    properties = ['name']


class ConferenceDetailEncoder(ModelEncoder):
    model = Conference
    properties = [
        "name",
        "description",
        "max_presentations",
        "max_attendees",
        "starts",
        "ends",
        "created",
        "updated",
        'location',
        
    ]
    
    encoders = {
        'location': LocationListEncoder(),
    }


@require_http_methods(['DELETE', 'GET', 'PUT'])
def api_show_conference(request, id):
    """
    Returns the details for the Conference model specified
    by the id parameter.

    This should return a dictionary with the name, starts,
    ends, description, created, updated, max_presentations,
    max_attendees, and a dictionary for the location containing
    its name and href.

    {
        "name": the conference's name,
        "starts": the date/time when the conference starts,
        "ends": the date/time when the conference ends,
        "description": the description of the conference,
        "created": the date/time when the record was created,
        "updated": the date/time when the record was updated,
        "max_presentations": the maximum number of presentations,
        "max_attendees": the maximum number of attendees,
        "location": {
            "name": the name of the location,
            "href": the URL for the location,
        }
    }
    """


    if request.method=='GET':
        conference = Conference.objects.get(id=id)
        weather_conditions = weather(conference.location.city, conference.location.state.name)
        #print(weather_conditions)
        
       
        return JsonResponse(
            {'conference': conference, 'weather_conditions': weather_conditions},
            encoder=ConferenceDetailEncoder,
            safe=False,
        )
    elif request.method == 'DELETE':
        count, _ = Conference.objects.filter(id=id).delete()
        return JsonResponse({'deleted': count > 0})


    

    else:
        #copied from create
        content = json.loads(request.body)
        #convert submitted JSON-formatted str to dict
        try:
            #new code
            if 'location' in content:
                location = Location.objects.get(name=content['location'])
                content['location'] = location
                
        except Location.DoesNotExist:
            return JsonResponse(
                { 'message': 'Invalid conference location' },
                    status=400,
            )

        
        


    
    Conference.objects.filter(id=id).update(**content)

    conference = Conference.objects.get(id=id)
    return JsonResponse(
        conference,
        encoder=ConferenceDetailEncoder,
        safe=False,
    )
    
@require_http_methods(['GET', 'POST'])
#only applicable if request.method is equal to 'GET'.
def api_list_locations(request):
    """
    Lists the location names and the link to the location.

    Returns a dictionary with a single key "locations" which
    is a list of location names and URLS. Each entry in the list
    is a dictionary that contains the name of the location and
    the link to the location's information.

    {
        "locations": [
            {
                "name": location's name,
                "href": URL to the location,
            },
            ...
        ]
    }
    """

    '''
    locations = [
        
        {
            'name': location.name,
            'href': location.get_api_url()
            
        }
        for location in Location.objects.all()
    ]
    '''
    if request.method == 'GET':
        locations = Location.objects.all()
        return JsonResponse(
            {'locations': locations},
            encoder=LocationListEncoder,
        )
    else:
        content = json.loads(request.body)
        try:
            #get the State object and put it into the content dict
            state = State.objects.get(abbreviation=content['state'])
            content['state'] = state
            #set content['state'] to the state's abbreviation
        except State.DoesNotExist:
            return JsonResponse(
                {'message': 'Invalid state abbreviation' },
                status=400,
            )



        #get picture url from pexels API
        state_full_name = state.name

        picture_url = get_picture_url(f'{content["city"], {state_full_name}}')
        

        print(picture_url)

        content['picture_url'] = picture_url






        #takes the request content and converts it to JSON
        location = Location.objects.create(**content)
        #create a new location instance with the request body
        return JsonResponse(
            location,
            encoder=LocationDetailEncoder,
            safe=False,
        )

class LocationDetailEncoder(ModelEncoder):
    model = Location
    properties = [
        'name', 
        'city',
        'room_count',
        'created',
        'updated',
        'picture_url',
        
    ]

    def get_extra_data(self, o):
        return { 'state': o.state.abbreviation }




@require_http_methods(['DELETE', 'GET', 'PUT'])
def api_show_location(request, id):
    """
    Returns the details for the Location model specified
    by the id parameter.

    This should return a dictionary with the name, city,
    room count, created, updated, and state abbreviation.

    {
        "name": location's name,
        "city": location's city,
        "room_count": the number of rooms available,
        "created": the date/time when the record was created,
        "updated": the date/time when the record was updated,
        "state": the two-letter abbreviation for the state,
    }
    """
    if request.method=='GET':
        location = Location.objects.get(id=id)
        return JsonResponse(
            location,
            encoder=LocationDetailEncoder,
            safe=False,
        )
    elif request.method == 'DELETE':
        count, _ = Location.objects.filter(id=id).delete()
        return JsonResponse({'deleted': count > 0})

    else:
        #copied from create
        content = json.loads(request.body)
        #convert submitted JSON-formatted str to dict
        try:
            #new code
            if 'state' in content:
                state = State.objects.get(abbreviation=content['state'])
                content['state'] = state
                #retrieve state content from State and set content['state'] equal to the value of the state key
        except State.DoesNotExist:
            return JsonResponse(
                { 'message': 'Invalid state abbreviation' },
                    status=400,
            )
    Location.objects.filter(id=id).update(**content)
    #updates the content to match the above try/except
    location = Location.objects.filter(id=id)
    return JsonResponse(
        location, 
        encoder=LocationDetailEncoder,
        safe=False
    )
    #return updated location
